import pl.icedev.rmi.RMIClient;
import pl.icedev.rmi.RMIServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;

public class TestCS {

    public static void main(String[] args) throws IOException {
        final RMIServer serv = new RMIServer(new InetSocketAddress(2888));
        serv.registerInterface(new AwesomeServiceImpl(), AwesomeService.class);

        new Thread(new Runnable() {
            public void run() {
                try {
                    System.out.println("Listening.");
                    while (true) {
                        serv.update(-1);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        RMIClient rmi = new RMIClient("127.0.0.1", 2888);
        System.out.println("Connection to rmi");
        AwesomeService service = rmi.requestInterface(AwesomeService.class);

        String what = service.serve("Something");

        System.out.println("client got: " + what);
        String wut = service.serves(new String[]{"a", "b", "c", "d"});

        System.out.println("client got: " + wut);

        Map<String, Object> map = service.test("random123", 172, (float) 93.22);
        System.out.println(map.get("arg1"));
        System.out.println(map.get("arg2"));
        System.out.println(map.get("arg3"));
        System.out.println(map.get("arg4"));
        System.out.println(((Map<String, Object>) map.get("arg4")).get("we_need_to"));
        
        service.exception("arg");

    }


    public interface AwesomeService {
        public String serve(String what);

        public String serves(String[] what);
        
        public String exception(String msg);

        public Map<String, Object> test(String arg1, int arg2, float arg3);
    }

    static class AwesomeServiceImpl implements AwesomeService {
        public String serve(String what) {
            return what + " got served.";
        }

        @Override
        public String serves(String[] what) {
            String res = "";
            for (String s : what) {
                res += s;
            }
            return res;
        }

        public Map<String, Object> test(String arg1, int arg2, float arg3) {
            Map<String, Object> out = new HashMap<>();
            Map<String, Object> out2 = new HashMap<>();
            out2.put("we_need_to", "go_deeper");
            out.put("arg1", arg1);
            out.put("arg2", arg2);
            out.put("arg3", arg3);
            out.put("arg4", out2);
            return out;
        }

		@Override
        public String exception(String msg) {
			throw new IllegalArgumentException(msg);
        }
    }
}
