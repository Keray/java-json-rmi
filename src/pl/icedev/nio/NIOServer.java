package pl.icedev.nio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;

public class NIOServer<T extends NIOConnection> {
    private ServerSocketChannel channel;
    private Selector selector;

    //List<NIOConnection> connections = new ArrayList<>();
    private final NIOFactory<T> factory;

    public NIOServer(InetSocketAddress addr, NIOFactory<T> factory) throws IOException {
        this.factory = factory;
        selector = Selector.open();
        channel = selector.provider().openServerSocketChannel();
        channel.socket().bind(addr);
        channel.configureBlocking(false);
        channel.register(selector, SelectionKey.OP_ACCEPT);
    }

    private int select(int timeout) throws IOException {
        if (timeout == 0)
            return selector.selectNow();
        if (timeout == -1)
            return selector.select();
        return selector.select(timeout);
    }

    public void update(int timeout) throws IOException {
        int num = select(timeout);
        if (num == 0)
            return;

        Iterator<SelectionKey> keys = selector.selectedKeys().iterator();

        while (keys.hasNext()) {
            SelectionKey key = keys.next();
            keys.remove();

            if (key.isAcceptable()) {
                SocketChannel accept = channel.accept();
                if (accept != null) {
                    System.out.println("Accepted: " + accept.getRemoteAddress());

                    accept.configureBlocking(false);
                    factory.newConnection(accept, selector);
                }
            }

            if (key.isReadable() || key.isWritable()) {
                NIOConnection con = (NIOConnection) key.attachment();
                if (con == null) {
                    key.channel().close();
                    continue;
                }
                try {
                    if (key.isReadable()) {
                        for (; ; ) {
                            String packet = con.readPacket();
                            if (packet == null)
                                break;
                            con.onMessage(packet);
                        }
                    }
                    if (key.isWritable()) {
                        con.write();
                    }
                } catch (Exception e) {
                    con.onDisconnect();
                    con.close();
                    if (!(e instanceof SocketException)) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

}
