package pl.icedev.nio;

import java.io.IOException;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;

public interface NIOFactory<T extends NIOConnection> {
    public T newConnection(SocketChannel accept, Selector selector) throws IOException;
}
