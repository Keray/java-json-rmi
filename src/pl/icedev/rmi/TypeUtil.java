package pl.icedev.rmi;

import org.json.simple.JSONArray;

import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.Map;

public class TypeUtil {

    private static final Map<Class, Character> tch = new HashMap<Class, Character>();

    static {
        tch.put(String.class, 'S');

        tch.put(Boolean.class, 'B');
        tch.put(boolean.class, 'B');

        tch.put(Integer.class, 'I');
        tch.put(int.class, 'I');
        tch.put(Long.class, 'I');
        tch.put(long.class, 'I');

        tch.put(Float.class, 'F');
        tch.put(float.class, 'F');
        tch.put(Double.class, 'F');
        tch.put(double.class, 'F');
    }

    public static char getTypeChar(Object obj) {
        if (obj == null)
            return 'O';
        return getTypeChar(obj.getClass());
    }

    public static char getTypeChar(Class<?> type) {
        Character c = tch.get(type);
        if (c != null)
            return c;
//		if(type.isArray())
//			return 'A';
        return 'O';
    }

    /**
     * converts Long/Integer to int, otherwise fails
     */
    public static int toInt(Object o) {
        if (o instanceof Long)
            return ((Long) o).intValue();
        return (Integer) o;
    }

    /**
     * converts Double/Float to float, otherwise fails
     */
    public static float toFloat(Object o) {
        if (o instanceof Double)
            return ((Double) o).floatValue();
        return (Float) o;
    }

    @SuppressWarnings("unchecked")
    public static Object[] toArray(Object obj, Class<?> type) {
        if (obj instanceof JSONArray) {
//            for (Object o : (JSONArray) obj) {
//                System.out.println("[] = " + o + " / " + o.getClass());
//            }
            Object[] array = (Object[]) Array.newInstance(type, ((JSONArray) obj).size());
//            System.out.println("Created array: " + array);

            return ((JSONArray) obj).toArray(array);
        }
        return (Object[]) obj;
    }
}
