package pl.icedev.rmi;

public class RMIException extends RuntimeException {
    private RMIFault fault;

    public RMIException(RMIFault fault) {
        super();
        this.fault = fault;
    }

    public RMIException(Throwable t) {
        super(t);
        this.fault = RMIFault.EXCEPTION;
    }

    public RMIException(String faultName) {
        super();
        try {
            fault = RMIFault.valueOf(faultName);
        } catch (IllegalArgumentException e) {
            fault = RMIFault.UNKNOWN;
        }
    }

    public RMIFault getFault() {
        return fault;
    }

    @Override
    public String getMessage() {
        return fault.name();
    }
}
