package pl.icedev.rmi;

public enum RMIFault {
    UNKNOWN,
    EXCEPTION,
    INVALID_MESSAGE,
    INTERNAL_ERROR,
    ITF_NOT_IMPLEMENTED,
    ITF_ID_NOT_FOUND;
}
