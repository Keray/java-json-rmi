package pl.icedev.rmi;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.*;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.Socket;

public class RMIClient implements Closeable {
    JSONParser parser;

    Socket sock;

    private BufferedReader in;
    private PrintStream out;

    public RMIClient(String host, int port) throws IOException {
        parser = new JSONParser();

        sock = new Socket(host, port);
        in = new BufferedReader(new InputStreamReader(sock.getInputStream(), "UTF8"));
        out = new PrintStream(sock.getOutputStream(), true, "UTF8");
    }


    private synchronized JSONObject send(JSONObject obj) throws IOException {
        if (sock == null) {
            throw new IllegalStateException("Socket is already closed.");
        }

        String str = obj.toJSONString();
        out.println(str);
        out.flush();
        String line = in.readLine();
        try {
            return (JSONObject) parser.parse(line);
        } catch (Exception e) {
            throw new IOException(e);
        }
    }

    @SuppressWarnings("unchecked")
    public <T> T requestInterface(Class<T> clazz) {
        String itname = clazz.getSimpleName();
        JSONObject json = new JSONObject();
        json.put("request", itname);

        try {
            JSONObject resp = send(json);

            if (resp.containsKey("error")) {
                throw new RMIException((String) resp.get("error"));
            }


            Integer id = (int) (long) resp.get("id");

            // TODO check if interface is correct (method signatures)

            return (T) Proxy.newProxyInstance(clazz.getClassLoader(), new Class[]{clazz}, new RMIProxy(id));

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() throws IOException {
        sock.close();
        sock = null;
    }

    private class RMIProxy implements InvocationHandler {

        Integer id;

        public RMIProxy(Integer id) {
            this.id = id;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            JSONObject json = new JSONObject();
            json.put("id", id);
            json.put("method", method.getName());
            json.put("args", args);

            json = send(json);

            if (json.containsKey("error")) {
                throw new RMIException((String) json.get("error"));
            }

            Object result = json.get("result");

            return result;
        }

    }

}
