package pl.icedev.util;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.NoSuchElementException;

public class StringMap {
    private JSONObject json;

    public StringMap(JSONObject json) {
        this.json = json;
    }

    public String getString(String key) {
        Object obj = json.get(key);
        if (obj == null) {
            throw new NoSuchElementException(key);
        }

        try {
            return (String) obj;
        } catch (ClassCastException e) {
            throw new IllegalArgumentException(key, e);
        }
    }

    public int getInt(String key) {
        Object obj = json.get(key);
        if (obj == null) {
            throw new NoSuchElementException(key);
        }

        try {
            return (int) (long) obj;
        } catch (ClassCastException e) {
            throw new IllegalArgumentException(key, e);
        }
    }

    public long getLong(String key) {
        Object obj = json.get(key);
        if (obj == null) {
            throw new NoSuchElementException(key);
        }

        try {
            return (long) obj;
        } catch (ClassCastException e) {
            throw new IllegalArgumentException(key, e);
        }
    }

    public StringMap getMap(String key) {
        Object obj = json.get(key);
        if (obj == null) {
            throw new NoSuchElementException(key);
        }

        try {
            return new StringMap((JSONObject) obj);
        } catch (ClassCastException e) {
            throw new IllegalArgumentException(key, e);
        }
    }

    public JSONArray getArray(String key) {
        Object obj = json.get(key);
        if (obj == null) {
            throw new NoSuchElementException(key);
        }

        try {
            return (JSONArray) obj;
        } catch (ClassCastException e) {
            throw new IllegalArgumentException(key, e);
        }
    }

    public JSONObject getRawObject() {
        return json;
    }
}
